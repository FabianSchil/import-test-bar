# Welcome to the Virtual Bar of the Methods and Tools Team!
![map](./public/map.png)
This Map was created with [WorkAdventure](https://workadventu.re).

Before you join make sure you chek out our [How-to](https://siemens.sharepoint.com/:p:/r/teams/mobilitydevops/Shared%20Documents/Communication%20Material/How%20to%20connect%20to%20Work%20Adventure.pptx?d=w80bc1beb34fd421bbaec65257864b131&csf=1&web=1&e=NNonSc) for the Virtual Bar.  
Join us [here](https://play.workadventu.re/_/public/fabian.schilling.code.siemens.io/workadventure-mnt/map.json)!

You want to build your own map? Follow these few steps [https://workadventu.re/map-building](https://workadventu.re/map-building) from Work Adventure.
Test your own map with the one created in your Settings > Pages.

## Create your own virtual bar (Work in Progress)

1. Fork this project
2. Set the gh-pages branch as default branch
3. Run the pipeline
4. When the Pipeline succeds a link appears in "Seetings > Pages"
5. Now you can test your map.
6. Configure this link depending on your Username and Repositoryname:  
   https://play.bar.siemens.chat/_/public/YOUR-USERNAME.code.siemens.io/REPOSITORY-NAME/map.json  
7. Send you configured link to your collegues and enjoy your time at the Virtual Bar!

## Licenses

This project contains multiple licenses:

* [Code license](./LICENSE.code) *(all files except those for other licenses)*
* [Map license](./LICENSE.map) *(`map.json` and the map visual as well)*
* [Assets license](./LICENSE.assets) *(the files inside the `src/assets/` folder)*

### About third party assets

If you add third party assets in your map, do not forget to:
1. Credit the author and license with the "tilesetCopyright" property present in the properties of each tilesets in the `map.json` file
2. Add the license text in LICENSE.assets
